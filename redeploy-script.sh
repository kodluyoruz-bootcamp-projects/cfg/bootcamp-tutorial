#!/bin/bash

kubectl patch deployment ${CI_PROJECT_NAME}-deployment -p '{"spec": {"template": {"metadata": {"labels": {"redeploy": "'$(date +%s)'"}}}}}' -n $K8S_NAMESPACE
kubectl rollout status deployments/${CI_PROJECT_NAME}-deployment -n=$K8S_NAMESPACE
